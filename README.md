## trinket-user 10 QKQ1.200209.002 release-keys
- Manufacturer: realme
- Platform: trinket
- Codename: realme_trinket
- Brand: Realme
- Flavor: lineage_realme_trinket-userdebug
- Release Version: 12
- Id: SQ1D.220205.003
- Incremental: b52c3a5807
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: realme/RMX1911/RMX1911:10/QKQ1.200209.002/1608537052:user/release-keys
- OTA version: 
- Branch: trinket-user-10-QKQ1.200209.002-release-keys
- Repo: realme_realme_trinket_dump_23850


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
